#include "Miner_field.cpp"

int main() {
    int h, w, k;
    srand(time(NULL));
    do {
        std::cout << "Enter sizes of field to generate: width(<=100) and height(<=100):\n";
        std::cin >> w >> h; 
    }
    while ((w <= 0) || (h <= 0) || (w >= 100) || (h >= 100));
    do {
        std::cout << "Enter the number of mines to place at field(<=" << h * w << "):\n";
        std::cin >> k;
    }
    while ((k < 0) || (k > h* w));
    MinerField* matrixField = new MinerField(h, w);//генерация поля размеров высота*ширина
    unsigned int startTime = clock();
    matrixField->Generate(k);
    unsigned int endTime = clock();
    std::cout << "How would u like to get result?\n1 - from console\n2 - from file\n0 - Don't print anythin'!\n";
    std::cin >> k;
    switch (k){
        case EV_OUT_TYPE_CONSOLE:{
            std::cout << "The result of programm:\n";
            matrixField->PrintToConsole();
            break;
        }
        case EV_OUT_TYPE_FILE:{
            std::string path;
            do {
                std::cout << "Enter the right path for the file to be created at:\n";
                
                std::cin >> path;
            }
            while(path == "");
            matrixField->PrintToFile(path);
            std::cout << "The result of the programm is located at the file at: "<< path<<"\n";
            break;
            }
        case EV_OUT_DEFAULT:{
            break;
        }
    }
    delete matrixField;
    std::cout << "\n";
    std::cout << "\nprocess finished after " << endTime - startTime << "ms\n";
    system("pause");
    return 0;
}
