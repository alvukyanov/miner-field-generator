#ifndef MINER_FIELD_H
#define MINER_FIELD_H
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<fstream>
#include<string>

#include<iostream>
#include<cstdlib>
#include<ctime>
#include<fstream>
#include<string>

enum E_OUT_TYPE{  //перечисление значений возможных способов вывода
    EV_OUT_TYPE_CONSOLE = 1,
    EV_OUT_TYPE_FILE = 2,
    EV_OUT_DEFAULT = 0
};

class MinerField {
public:
    MinerField(int h, int w);//конструктор для поля, изначально зануляем, после в Generate на нули записываем *, если мина есть
    void Generate(int k);//генерация матрицы с минами, где k - количество мин, задаётся пользователем
    void PrintToConsole();//вывод поля
    void PrintToFile(std::string& path);

private:
    char* mField;
    void MDoFieldEdit(int x);//заполняем элемент матрицы числом мин вокруг неё, если она содержит 0 или другое число
    int MGetMinePosition(int max);//генерация координаты-положения мины в поле mField
    int mHeight;
    int mWidth;
    int mMapSize;
};

#endif
