cmake_minimum_required(VERSION 3.16)
project(miner_field_generator)

set(CMAKE_CXX_STANDARD 14)

add_executable(miner_field_generator
        main.cpp)
