#include "miner_field.h"

MinerField::MinerField(int h, int w)
{
    mHeight = h;
    mWidth = w;
    mMapSize = mHeight * mWidth;
    mField = new char[mMapSize];
    for (int i = 0; i < mMapSize; i++)
        mField[i] = '0';
}

void MinerField::Generate(int k) {
    while (k != 0) {
        int x = M123GetMinePosition(mMapSize);
        if (mField[x] != '*') {
            mField[x] = '*';
            MDoFieldEdit(x);
            k--;
        }
    }
}

void MinerField::PrintToConsole() {
    for (int i = 0; i < mHeight; i++) {
        for (int j = 0; j < mWidth; j++)
            std::cout << mField[i * mWidth + j] << " ";
        std::cout << "\n";
    }
}

void MinerField::PrintToFile(std::string &path) {
    std::ofstream fout;
    fout.open(path);
    fout.clear();
      for (int i = 0; i < mHeight; i++) {
        for (int j = 0; j < mWidth; j++)
            fout << mField[i * mWidth + j] << " ";
        fout << "\n";
    }
    fout.close();
}

void MinerField::MDoFieldEdit(int x) {
    const int neiboursQuantity = 8;
    int mFieldchek[neiboursQuantity] = {
       x - 1, x - 1 - mWidth, x - mWidth, x + 1 - mWidth,
       x + 1, x - 1 + mWidth, x + mWidth , x + 1 + mWidth };
    for (int i = 0; i < neiboursQuantity; i++) {
        if ((mFieldchek[i] < (mHeight * mWidth)) && (mFieldchek[i] >= 0) &&
            (mField[mFieldchek[i]] != '*') && (abs(x % mWidth - mFieldchek[i] % mWidth) <= 1)
            && (abs(x / mWidth - mFieldchek[i] / mWidth) <= 1))
            mField[mFieldchek[i]]++;
    }
}

int MinerField::MGetMinePosition(int max) {
    return (rand() % (max));
}
